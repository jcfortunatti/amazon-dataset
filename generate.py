from PIL import Image
from settings import tiles
from utils import Load, Save, CreateIfNotExists

def simplifyImage(map, startYear, endYear):
    template = Load('./template.json')

    sections = template['sections']
    matrix = template['matrix']

    original = Image.open('./decoded/{}/{}-{}.png'.format(map, startYear, endYear))
    width, height = original.size

    dx = int(width / sections)
    dy = int(height / sections)

    image = Image.new('RGBA', original.size, (0, 0, 0, 0))
    result = []

    for row in matrix:
        for col in row:
            i = col['x']
            j = col['y']
        
            r = None
            g = None
            b = None
            a = 0
            size = 0

            if col['z'] > 0:
                for _x in range(i * dx, (i + 1) * dx):
                    for _y in range(j * dy, (j + 1) * dy):
                        x = int(_x + dx / 2)
                        y = int(_y + dy / 2)

                        if x < width and y < height:
                            size += 1
                            pixel = original.getpixel((x, y))
                            if r == None:
                                r = pixel[0]
                                g = pixel[1]
                                b = pixel[2]
                                a = pixel[3]
                            else:
                                r = r + pixel[0]
                                g = g + pixel[1]
                                b = b + pixel[2]
                                a = a + pixel[3]

                r = r / size
                g = g / size
                b = b / size
                a = a / size

                for x in range(i * dx, (i + 1) * dx):
                    for y in range(j * dy, (j + 1) * dy):
                        image.putpixel((x, y), (int(r),int(g),int(b),int(a)))
            
            result.append({
                'x': col['x'],
                'y': col['y'],
                'z': round(a / 256, 2),
                'r': 'rainforest' in col,
            })
            

    Save(result, './generate/{}/{}-{}.json'.format(map, startYear, endYear))
    image.save('generate/{}/{}-{}.png'.format(map, startYear, endYear))

def generateJson(map):
    start_year = tiles[map]['start_year']
    end_year = tiles[map]['end_year']

    CreateIfNotExists('./generate')
    CreateIfNotExists('./generate/{}'.format(map))

    for i in range(0, int(end_year - start_year) + 1):
        simplifyImage(map, start_year, start_year + i)