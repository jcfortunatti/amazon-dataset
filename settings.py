tiles = {
    'forest': {
        'url': 'https://earthengine.google.org/static/hansen_2014/gfw_loss_tree_year_30_2014/{}/{}/{}.png',
        'start_year': 2001.,
        'end_year': 2001.,
    },
    'tree_loss': {
        'url': 'https://tiles.globalforestwatch.org/umd_tree_cover_loss/v1.8/tcd_75/{}/{}/{}.png',
        'start_year': 2001.,
        'end_year': 2020.,
    },
    'raster': {
        'url': 'https://s.basemaps.cartocdn.com/rastertiles/light_all/{}/{}/{}.png',
        'start_year': 2001.,
        'end_year': 2001.,
    }
}

tile_size = 256

zoom = 6
x0 = 17
x1 = 26
y0 = 30
y1 = 40