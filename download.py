import requests
import shutil
from utils import CreateIfNotExists

from settings import tiles, zoom, x0, x1, y0, y1

def downloadImage(map, x, y, z):
    image_url = tiles[map]['url'].format(z, x, y)
    filename = "./images/{}/{}-{}-{}.png".format(map, z, x, y)

    r = requests.get(image_url, stream=True)

    if r.status_code == 200:
        r.raw.decode_content = True
        with open(filename, 'wb') as f:
            shutil.copyfileobj(r.raw, f)

def downloadImages(map):
    CreateIfNotExists('./images')
    CreateIfNotExists('./images/{}'.format(map))

    for x in range(x0, x1):
        for y in range(y0, y1):
            downloadImage(map, x, y, zoom)