from PIL import Image
from settings import tiles
from utils import CreateIfNotExists
from utils import Load, Save

def concatJson(map):
    start_year = tiles[map]['start_year']
    end_year = tiles[map]['end_year']

    result = []
    
    for i in range(0, int(end_year - start_year) + 1):
        startYear = start_year
        endYear = start_year + i

        rows = Load('./generate/{}/{}-{}.json'.format(map, startYear, endYear))
        _rows = []
        for row in rows:
            if row['r']:
                _rows.append(row)

        result.append(_rows)
    
    Save(result, './concatenated.json')