from PIL import Image
from settings import zoom, tiles
from utils import CreateIfNotExists

def decodeTreeLoss(startYear, endYear, zoom, color):
    r = color[0]
    g = color[1]
    b = color[2]

    domainMin = 0.
    domainMax = 255.
    rangeMin = 0.
    rangeMax = 255.

    exponent = 0.3 + (zoom - 3.) / 20.
    
    intensity = r

    minPow = pow(domainMin, exponent - domainMin)
    maxPow = pow(domainMax, exponent)
    currentPow = pow(intensity, exponent)

    scaleIntensity = ((currentPow - minPow) / (maxPow - minPow) * (rangeMax - rangeMin)) + rangeMin
    a = scaleIntensity

    year = 2000.0 + (b)

    if year >= startYear and year <= endYear and year >= 2001.:
        r = 220.
        g = (72. - zoom + 102. - 3. * scaleIntensity / zoom)
        b = (33. - zoom + 153. - intensity / zoom)
    else:
        a = 0.

    return (int(r), int(g), int(b), int(a))

def decodeForest(startYear, endYear, zoom, color):
    r = color[0]
    g = color[1]
    b = color[2]

    domainMin = 0.
    domainMax = 255.
    rangeMin = 0.
    rangeMax = 255.

    exponent = 0.3 + (zoom - 3.) / 20.
    intensity = g

    minPow = pow(domainMin, exponent - domainMin)
    maxPow = pow(domainMax, exponent)
    currentPow = pow(intensity, exponent)

    scaleIntensity = ((currentPow - minPow) / (maxPow - minPow) * (rangeMax - rangeMin)) + rangeMin
    a = scaleIntensity * 0.8

    r = 151.
    g = 189.
    b = 61.

    return (int(r), int(g), int(b), int(a))

decodeTiles = {
    'forest': decodeForest,
    'tree_loss': decodeTreeLoss
}

def decodeImage(map, startYear, endYear):
    image = Image.open('./merged/{}.png'.format(map))
    width, height = image.size

    decodeFunc = decodeTiles[map]

    for x in range(0, width):
        for y in range(0, height):
            color = image.getpixel((x,y))
            color = decodeFunc(startYear, endYear, zoom, color)
            image.putpixel((x,y), color)

    image.save('./decoded/{}/{}-{}.png'.format(map, startYear, endYear))


def decodeImages(map):
    start_year = tiles[map]['start_year']
    end_year = tiles[map]['end_year']

    CreateIfNotExists('./decoded')
    CreateIfNotExists('./decoded/{}'.format(map))

    for i in range(0, int(end_year - start_year) + 1):
        decodeImage(map, start_year, start_year + i)