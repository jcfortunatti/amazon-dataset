from PIL import Image
from settings import zoom, x0, x1, y0, y1, tile_size
from utils import CreateIfNotExists

def mergeImages(map):
    CreateIfNotExists('./merged')

    image = Image.new('RGBA', ((x1 - x0) * tile_size, (y1 - y0) * tile_size), (0, 0, 0, 0))

    for x in range(x0, x1):
        for y in range(y0, y1):
            sectionImage = Image.open('./images/{}/{}-{}-{}.png'.format(map, zoom, x, y))

            width, height = sectionImage.size

            image.paste(sectionImage, ((x-x0) * width, (y-y0) * height, ((x-x0) + 1) * width, ((y-y0) + 1) * height))
    
    
    image.save('./merged/{}.png'.format(map))