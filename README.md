How to use:

1. Install the requirements
2. Run the following commands in order:
    1. python main.py --download forest|tree_loss
    2. python main.py --merge forest|tree_loss
    3. python main.py --decode forest|tree_loss
    4. python main.py --generate forest|tree_loss