import io
import json
import os

def Load(path):
    file = io.open('{}'.format(path), mode='r',
                   encoding='utf8', errors='ignore')
    return json.load(file)

def Save(data, path, indent=4):
    with io.open(path, "w", encoding='utf8') as file_handle:
        data = json.dumps(data, indent=indent, sort_keys=True, ensure_ascii=False)
        file_handle.write(data)

def CreateIfNotExists(path):
    if not os.path.exists(path):
        os.makedirs(path)