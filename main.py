import sys
import argparse
from download import downloadImages
from merge import mergeImages
from decode import decodeImages
from generate import generateJson
from concat import concatJson

def parse_args(args):
    parser = argparse.ArgumentParser(description="Available commands")
    parser.add_argument("--download", required=False, default=None)
    parser.add_argument("--merge", required=False, default=None)
    parser.add_argument("--decode", required=False, default=None)
    parser.add_argument("--generate", required=False, default=None)
    parser.add_argument("--concat", required=False, default=None)
    return parser.parse_args(args)

def main():
    args = parse_args(sys.argv[1:])

    if args.download != None:
        downloadImages(args.download)
    elif args.merge != None:
        mergeImages(args.merge)
    elif args.decode != None:
        decodeImages(args.decode)
    elif args.generate != None:
        generateJson(args.generate)
    elif args.concat != None:
        concatJson(args.concat)

if __name__ == '__main__':
    main()